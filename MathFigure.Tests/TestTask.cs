﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using NUnit.Framework;

namespace MathFigure.Tests
{
    public class TreeNode
    {
        public IEnumerable<TreeNode> Children { get; set; }
    }

    public class TestTask
    {
        public IEnumerable<TreeNode> GetAllNodes(TreeNode node)
        {
            if (node == null)
                throw new ArgumentNullException(nameof(node));

            var result = new List<TreeNode> {node};

            if (node.Children != null)
                foreach(var child in node.Children)
                    result.AddRange(GetAllNodes(child));
            return result;
        }

        [Test]
        public void TestMethod()
        {
            var childNodes = new List<TreeNode>()
            {
                new TreeNode(),
                new TreeNode()
            };
            var node = new TreeNode() {Children = childNodes};

            var result = GetAllNodes(node);
            Assert.AreEqual(3,result);
        }
    }
}
