using System;
using NUnit.Framework;

namespace MathFigure.Tests
{
    public class MathFigureTests
    {
        [Test]
        public void MathFigureValidationTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new CircleFigure(-2); });
        }

        [Test]
        public void CircleFigureSquareTest()
        {
            var figure = new CircleFigure(10);
            Assert.AreEqual(314, Math.Round(figure.Square()));
        }

        [Test]
        public void TriangleFigureValidationTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new TriangleFigure(3, 4, 10); });
        }

        [Test]
        public void TriangleFigureSquareTest()
        {
            var figure = new TriangleFigure(3, 4, 5);
            Assert.AreEqual(6, Math.Round(figure.Square()));
        }

        [Test]
        public void TriangleFigureTypeTest()
        {
            var figure = new TriangleFigure(3, 4, 5);
            Assert.IsTrue(figure.IsRectangular);
        }
    }
}