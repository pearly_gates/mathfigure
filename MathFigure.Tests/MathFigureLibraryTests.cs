using System;
using NUnit.Framework;

namespace MathFigure.Tests
{
    public class MathFigureLibraryTests
    {
        [Test]
        public void MathFigureLibraryTest()
        {
            var lib = new MathFigureLibrary();
            
            Assert.AreEqual(6, Math.Round(lib.CalculateTriangleSquare(3, 4, 5)));
        }

        [Test]
        public void TriangleFigureValidationTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new TriangleFigure(3, 4, 10); });
        }
    }
}