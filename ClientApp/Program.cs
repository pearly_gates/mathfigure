﻿using System;
using MathFigure;

namespace ClientApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var library = new MathFigureLibrary();
            Console.WriteLine($"Площадь круга с R=10 :" +
                              $" {library.CalculateCircleSquare(10)}");
            Console.WriteLine($"Площадь треугольника со сторонами 3,4,5 :" +
                              $" {library.CalculateTriangleSquare(3,4,5)}");
            Console.WriteLine($"Является ли треугольник со сторонами 3,4,5 прямоугольным :" +
                              $" {library.IsRectangularTriangle(3, 4, 5)}");
            Console.WriteLine($"Площадь фигуры с одним параметром равным 100 (круг) :" +
                              $" {library.CalculateMathFigureSquare(new double[] {100})}");
            try
            {
                Console.WriteLine($"Площадь фигуры с двумя параметрами, равными 5 и 10 :" +
                                  $" {library.CalculateMathFigureSquare(new double[] { 5, 10})}");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Фигура с двумя параметрами не реализована");
            }
            Console.ReadLine();
        }
    }
}
