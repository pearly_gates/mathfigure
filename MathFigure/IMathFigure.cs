﻿namespace MathFigure
{
    public interface IMathFigure
    {
        double Square();
    }
}
