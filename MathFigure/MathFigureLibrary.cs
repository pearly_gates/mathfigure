﻿namespace MathFigure
{
    public class MathFigureLibrary
    {
        public double CalculateCircleSquare(double radius)
        {
            return MathFigureFactory.CreateCircleFigure(radius).Square();
        }

        public double CalculateTriangleSquare(double sideA, double sideB, double sideC)
        {
            return MathFigureFactory.CreateTriangleFigure(sideA, sideB, sideC).Square();
        }

        public double CalculateMathFigureSquare(double[] figureParams)
        {
            return MathFigureFactory.CreateMathFigure(figureParams).Square();
        }

        public bool IsRectangularTriangle(double sideA, double sideB, double sideC)
        {
            return MathFigureFactory.CreateTriangleFigure(sideA, sideB, sideC).IsRectangular;
        }
    }
}
