﻿using System;

namespace MathFigure
{
    public class TriangleFigure : PolygonFigure, IMathFigure
    {
        protected override int ParamCount => 3;
        private const double TOLERANCE = 0.00001;

        private double SideA => FigureParams[0];
        private double SideB => FigureParams[1];
        private double SideC => FigureParams[2];

        public bool IsRectangular => 
            (Math.Abs(Math.Pow(SideA, 2) - Math.Pow(SideB, 2) - Math.Pow(SideC, 2)) < TOLERANCE) ||
            (Math.Abs(Math.Pow(SideB, 2) - Math.Pow(SideA, 2) - Math.Pow(SideC, 2)) < TOLERANCE) ||
            (Math.Abs(Math.Pow(SideC, 2) - Math.Pow(SideA, 2) - Math.Pow(SideB, 2)) < TOLERANCE);

        public TriangleFigure(double sideA, double sideB, double sideC)
            : base(new[] {sideA, sideB, sideC})
        {
            if ((sideA + sideB <= sideC) || (sideA + sideC <= sideB) || (sideC + sideB <= sideA))
                throw new ArgumentOutOfRangeException("Невозможно построить треугольник с такими сторонами");
        }

        public new double Square()
        {
            var p = (SideA + SideB + SideC) / 2;
            return Math.Sqrt(p*(p-SideA)* (p - SideB)* (p - SideC));
        }
    }
}