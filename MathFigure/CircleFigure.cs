﻿using System;

namespace MathFigure
{
    /// <summary>
    /// Класс, представляющий круг
    /// </summary>
    public class CircleFigure : MathFigureBase, IMathFigure
    {
        protected override int ParamCount => 1;

        private double Radius => FigureParams[0];

        public CircleFigure(double radius) : base(new[] {radius})
        {
        }

        public double Square()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}