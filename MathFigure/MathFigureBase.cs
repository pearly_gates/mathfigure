﻿using System;

namespace MathFigure
{
    /// <summary>
    /// Абстрактный класс геометрической фигуры
    /// Осуществляет валидацию параметров
    /// </summary>
    public abstract class MathFigureBase
    {
        protected abstract int ParamCount { get; }
        protected double[] FigureParams;

        protected MathFigureBase(double[] figureParams)
        {
            ValidateParams(figureParams);

            FigureParams = figureParams;
        }

        private void ValidateParams(double[] figureParams)
        {
            if (figureParams.Length != ParamCount)
                throw new ArgumentOutOfRangeException(nameof(figureParams),
                    "Неверное количество параметров фигуры");

            foreach (var figureParam in figureParams)
                if (figureParam <= 0)
                    throw new ArgumentOutOfRangeException(nameof(figureParams),
                        "Параметры фигуры не могут быть отрицательными числами");
        }
    }
}