﻿using System;

namespace MathFigure
{
    /// <summary>
    /// Класс, представляющий многоугольник (для дальнейшего расширения модели)
    /// </summary>
    public class PolygonFigure : MathFigureBase, IMathFigure
    {
        protected override int ParamCount => throw new NotImplementedException("Неизвестная фигура");

        public PolygonFigure(double[] figureParams) : base(figureParams)
        {
        }

        public double Square()
        {
            throw new NotImplementedException();
        }
    }
}