﻿using System;

namespace MathFigure
{
    public static class MathFigureFactory
    {
        public static IMathFigure CreateMathFigure(double[] figureParams)
        {
            IMathFigure figure;

            switch (figureParams.Length)
            {
                case 0:
                    throw new ArgumentException("Пустой массив параметров фигуры", nameof(figureParams));
                case 1:
                    figure = new CircleFigure(figureParams[0]);
                    break;
                case 3:
                    figure = new TriangleFigure(figureParams[0], figureParams[1], figureParams[2]);
                    break;
                default:
                    throw new ArgumentException("Неизвестная фигура", nameof(figureParams));
            }

            return figure;
        }

        public static CircleFigure CreateCircleFigure(double radius)
        {
            return new CircleFigure(radius);
        }

        public static TriangleFigure CreateTriangleFigure(double sideA, double sideB, double sideC)
        {
            return new TriangleFigure(sideA, sideB, sideC);
        }
    }
}
